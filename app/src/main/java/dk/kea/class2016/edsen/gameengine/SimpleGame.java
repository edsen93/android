package dk.kea.class2016.edsen.gameengine;

/**
 * Created by emil on 17-02-2016.
 */
public class SimpleGame extends Game
{

    @Override
    public Screen createStartScreen()

    {
        return new SimpleScreen(this);
    }

}
