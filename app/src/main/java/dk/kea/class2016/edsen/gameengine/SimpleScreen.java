package dk.kea.class2016.edsen.gameengine;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;

import java.util.Random;

/**
 * Created by emil on 17-02-2016.
 */
public class SimpleScreen extends Screen
{
    Bitmap bitmap;
    int x = 0, y = 0;
    Random random = new Random();
    int clearColor = Color.BLUE;

    public SimpleScreen(Game game)
    {
        super(game);
        bitmap = game.loadBitmap("bob.png");
    }
    public void update(float deltaTime)
    {

        game.clearFrameBuffer(clearColor);
        game.drawBitmap(bitmap, 10, 10);
        game.drawBitmap(bitmap,100, 200, 0,0, 64, 64 );
        if (game.isKeyPressed(KeyEvent.KEYCODE_MENU))
        {

            clearColor = random.nextInt();
        }

    }


    public void pause()
    {
        Log.d("SimpleScreen", "We are pausing");
    }
    public void resume() {
        Log.d("SimpleScreen", "We are resuming");

    }
    public void dispose() {
        Log.d("SimpleScreen", "We are dispose");

    }

}