package dk.kea.class2016.edsen.gameengine;

/**
 * Created by emil on 17-02-2016.
 */
public enum  State
{
    Running,
    Paused,
    Resumed,
    Disposed
}
